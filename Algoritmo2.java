package algoritmotwo;

import java.util.ArrayList;
import java.util.List;

public class AlgoritmoTwo {

   
    public static void main(String[] args) {
      int n = 1000;
        List<Pair> resultado = miAlgoritmo(n);
    }
    
     public static List<Pair> miAlgoritmo(int n) {
        List<Pair> pares = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                pares.add(new Pair(i, j));
            }
        }
        return pares;
    } 
     static class Pair {
        int first;
        int second;

        Pair(int first, int second) {
            this.first = first;
            this.second = second;
        }
     }
}
